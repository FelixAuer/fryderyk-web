# frozen_string_literal: true

module ApplicationHelper
end

String.class_eval do
  def possessive
    return self if empty?

    self + (self[-1, 1] == 's' ? Possessive::APOSTROPHE_CHAR : "#{Possessive::APOSTROPHE_CHAR}s")
  end
end

module Possessive
  APOSTROPHE_CHAR = '’'
end
