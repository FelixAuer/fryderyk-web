# frozen_string_literal: true

json.extract! repertoire_piece, :id, :name, :composer, :level, :last_played, :user_id, :created_at, :updated_at
json.url repertoire_piece_url(repertoire_piece, format: :json)
