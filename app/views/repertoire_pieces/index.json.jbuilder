# frozen_string_literal: true

json.array! @repertoire_pieces, partial: 'repertoire_pieces/repertoire_piece', as: :repertoire_piece
