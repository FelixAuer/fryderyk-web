# frozen_string_literal: true

json.extract! practice_plan, :id, :created_at, :updated_at
json.url practice_plan_url(practice_plan, format: :json)
