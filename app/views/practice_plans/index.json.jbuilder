# frozen_string_literal: true

json.array! @practice_plans, partial: 'practice_plans/practice_plan', as: :practice_plan
