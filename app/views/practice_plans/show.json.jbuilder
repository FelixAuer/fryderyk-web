# frozen_string_literal: true

json.partial! 'practice_plans/practice_plan', practice_plan: @practice_plan
