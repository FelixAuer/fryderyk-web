# frozen_string_literal: true

json.array! @absence_periods, partial: 'absence_periods/absence_period', as: :absence_period
