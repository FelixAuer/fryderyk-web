# frozen_string_literal: true

json.partial! 'absence_periods/absence_period', absence_period: @absence_period
