# frozen_string_literal: true

json.extract! absence_period, :id, :starts_at, :ends_at, :user_id, :created_at, :updated_at
json.url absence_period_url(absence_period, format: :json)
