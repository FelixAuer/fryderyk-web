import { createApp } from 'vue/dist/vue.esm-bundler';
import PracticePlanEdit from "../javascript/components/practiceplan/PracticePlanEdit.vue";
import PracticePlanShow from "../javascript/components/practiceplan/PracticePlanShow.vue";

const app = createApp({
    data() {
        return {
        }
    }
})
app.component('PracticePlanEdit', PracticePlanEdit)
app.component('PracticePlanShow', PracticePlanShow)

app.mount('#app');
