# frozen_string_literal: true

class ApplicationController < ActionController::Base
  def initialize
    super
    @show_sidebar = true
  end

  def after_sign_in_path_for(_resource)
    practice_plans_path
  end

  def after_sign_out_path_for(_resource_or_scope)
    root_path
  end

  def show_sidebar?
    user_signed_in? && @show_sidebar
  end

  helper_method :show_sidebar?

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def hide_sidebar
    @show_sidebar = false
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[username email password])
    devise_parameter_sanitizer.permit(:account_update, keys: %i[username email password current_password])
  end

  class << self
    protected

    def require_permission(resource_name, options)
      # noinspection RubyResolve
      before_action options do
        unless current_user == instance_variable_get("@#{resource_name}").user
          redirect_to root_path,
                      status: :forbidden
        end
      end
    end
  end
end
