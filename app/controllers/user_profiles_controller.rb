# frozen_string_literal: true

class UserProfilesController < ApplicationController
  before_action :hide_sidebar

  def show # rubocop:todo Metrics/AbcSize, Metrics/MethodLength
    @user = User.where('lower(username) = ?', params[:username].downcase).first
    raise ActionController::RoutingError, 'Not Found' unless @user

    @today = @user.days.today.first.time_in_seconds
    last_seven_days = @user.days.last_7_days
    @sum7 = last_seven_days.to_a.reduce(0) { |sum, day| sum + day[:time_in_seconds] }
    @days_played = last_seven_days.count

    last30 = @user.days.last_30_days.sort_by(&:date).each_with_object({}) do |day, accum|
      accum[day.date] = day.time_in_seconds / 60
    end
    @last_30_days = ((Date.today - 29.days)..Date.today).each_with_object({}) do |date, hash|
      hash[date.to_s] = last30.fetch(date, 0)
    end
  end
end
