# frozen_string_literal: true

class GoalsController < ApplicationController
  before_action :set_goal, only: %i[show edit update]
  before_action :authenticate_user!
  require_permission :goal, only: %i[show edit update]

  # GET /goals or /goals.json
  def index
    current_user.daily_goal = DailyGoal.create(value: 20) unless current_user.daily_goal
    current_user.weekly_goal = WeeklyGoal.create(value: 100) unless current_user.weekly_goal
    @daily_goal = current_user.daily_goal
    @weekly_goal = current_user.weekly_goal
  end

  # GET /goals/1 or /goals/1.json
  def show; end

  # GET /goals/new
  def new
    @goal = Goal.new
  end

  # GET /goals/1/edit
  def edit; end

  # PATCH/PUT /goals/1 or /goals/1.json
  def update
    if @goal.update({ value: goal_params[:value].to_i * 60, active: goal_params[:active] })
      redirect_to goals_url, notice: 'Goal was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_goal
    @goal = Goal.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def goal_params
    params.require(:goal).permit(:value, :active)
  end
end
