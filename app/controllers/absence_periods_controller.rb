# frozen_string_literal: true

class AbsencePeriodsController < ApplicationController
  before_action :set_absence_period, only: %i[show edit update destroy]
  before_action :authenticate_user!
  require_permission :absence_period, only: %i[show edit update destroy]

  # GET /absence_periods or /absence_periods.json
  def index
    @absence_periods = AbsencePeriod.all
  end

  # GET /absence_periods/1 or /absence_periods/1.json
  def show; end

  # GET /absence_periods/new
  def new
    @absence_period = AbsencePeriod.new
  end

  # GET /absence_periods/1/edit
  def edit; end

  # POST /absence_periods or /absence_periods.json
  def create
    @absence_period = current_user.absence_periods.new(absence_period_params)

    respond_to do |format|
      if @absence_period.save
        format.html { redirect_to absence_periods_url, notice: 'Absence period was successfully created.' }
        format.json { render :show, status: :created, location: @absence_period }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @absence_period.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /absence_periods/1 or /absence_periods/1.json
  def update
    if @absence_period.update(absence_period_params)
      redirect_to absence_period_url(@absence_period), notice: 'Absence period was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /absence_periods/1 or /absence_periods/1.json
  def destroy
    @absence_period.destroy

    respond_to do |format|
      format.html { redirect_to absence_periods_url, notice: 'Absence period was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_absence_period
    @absence_period = AbsencePeriod.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def absence_period_params
    params.require(:absence_period).permit(:starts_at, :ends_at)
  end
end
