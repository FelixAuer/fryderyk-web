# frozen_string_literal: true

module ApiAuthentication
  extend ActiveSupport::Concern

  def authenticate_via_auth_token
    user = User.find_by(auth_token: params[:auth_token])
    raise ActionController::RoutingError, 'Not Permitted' if user.nil?

    sign_in(:user, user)
  end
end
