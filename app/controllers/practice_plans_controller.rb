# frozen_string_literal: true

class PracticePlansController < ApplicationController
  before_action :set_practice_plan, only: %i[show edit update destroy duplicate]
  before_action :authenticate_user!, except: %i[index show]
  before_action :hide_sidebar
  require_permission :practice_plan, only: %i[destroy], but: 'asdfa sd'

  # GET /practice_plans or /practice_plans.json
  def index
    @practice_plans = PracticePlan.all
  end

  # GET /practice_plans/1 or /practice_plans/1.json
  def show; end

  # GET /practice_plans/new
  def new
    @practice_plan = PracticePlan.new
  end

  # GET /practice_plans/1/edit
  def edit; end

  # POST /practice_plans or /practice_plans.json
  def create # rubocop:todo Metrics/AbcSize, Metrics/MethodLength
    params = practice_plan_params
    params[:user_id] = current_user.id
    @practice_plan = PracticePlan.new(params)

    respond_to do |format|
      if @practice_plan.save
        format.html do
          redirect_to edit_practice_plan_path(@practice_plan), notice: 'Practice plan was successfully created.'
        end
        format.json { render :show, status: :created, location: @practice_plan }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @practice_plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /practice_plans/1 or /practice_plans/1.json
  def update # rubocop:todo Metrics/AbcSize, Metrics/MethodLength
    respond_to do |format|
      updated = false
      ActiveRecord::Base.transaction do
        @practice_plan.practice_instructions.destroy_all
        updated = @practice_plan.update(practice_plan_params)
        raise ActiveRecord::Rollback unless updated
      end
      if updated
        format.html { redirect_to practice_plan_url(@practice_plan), notice: 'Practice plan was successfully updated.' }
        format.json do
          flash.notice = 'Practice plan was successfully updated.'
          render :show, status: :ok, location: practice_plans_path
        end
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @practice_plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /practice_plans/1 or /practice_plans/1.json
  def destroy
    @practice_plan.destroy

    respond_to do |format|
      format.html { redirect_to practice_plans_url, notice: 'Practice plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # POST /practice_plans/duplicate
  def duplicate
    @practice_plan.duplicate
    redirect_to practice_plans_url, notice: 'Practice plan was successfully duplicated.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_practice_plan
    @practice_plan = PracticePlan.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def practice_plan_params
    params.require(:practice_plan).permit(:name, :description, :intro, :duration, :tags, :show_to_guests,
                                          # rubocop:todo Layout/LineLength
                                          practice_instructions_attributes: %i[name description end_condition position seconds repetitions])
    # rubocop:enable Layout/LineLength
  end
end
