# frozen_string_literal: true

class RepertoirePiecesController < ApplicationController
  before_action :set_repertoire_piece,
                only: %i[show edit update destroy raise_level lower_level keep_level rollback_practice]
  before_action :authenticate_user!
  require_permission :repertoire_piece,
                     only: %i[show edit update destroy raise_level lower_level keep_level rollback_practice]

  # GET /repertoire_pieces or /repertoire_pieces.json
  def index
    @repertoire_pieces = current_user.repertoire_pieces.all
  end

  # GET /repertoire_pieces/1 or /repertoire_pieces/1.json
  def show; end

  # GET /repertoire_pieces/new
  def new
    @repertoire_piece = RepertoirePiece.new
  end

  # GET /repertoire_pieces/1/edit
  def edit; end

  # POST /repertoire_pieces or /repertoire_pieces.json
  def create
    @repertoire_piece = current_user.repertoire_pieces.new(repertoire_piece_params)

    respond_to do |format|
      if @repertoire_piece.save
        format.html { redirect_to repertoire_pieces_url, notice: 'Repertoire piece was successfully created.' }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /repertoire_pieces/1 or /repertoire_pieces/1.json
  def update
    respond_to do |format|
      if @repertoire_piece.update(repertoire_piece_params)
        format.html { redirect_to repertoire_pieces_url, notice: 'Repertoire piece was successfully updated.' }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  def raise_level
    @repertoire_piece.raise_level
    redirect_to dashboard_path,
                # rubocop:todo Layout/LineLength
                notice: "Good job, we'll wait a little longer to show you this piece again! Spend a few minutes polishing '#{@repertoire_piece.name}' if you like."
    # rubocop:enable Layout/LineLength
  end

  def lower_level
    @repertoire_piece.lower_level
    redirect_to dashboard_path,
                notice: "Time to revisit '#{@repertoire_piece.name}' for a little bit. We'll try again soon!"
  end

  def keep_level
    @repertoire_piece.keep_level
    redirect_to dashboard_path,
                notice: "Let's keep it up! We'll show you '#{@repertoire_piece.name}' after the same time again."
  end

  def rollback_practice
    @repertoire_piece.rollback_practice
    redirect_to dashboard_path, notice: "All clear, let's do this again!"
  end

  # DELETE /repertoire_pieces/1 or /repertoire_pieces/1.json
  def destroy
    @repertoire_piece.destroy

    respond_to do |format|
      format.html { redirect_to repertoire_pieces_url, notice: 'Repertoire piece was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_repertoire_piece
    @repertoire_piece = RepertoirePiece.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def repertoire_piece_params
    params.require(:repertoire_piece).permit(:name, :composer, :level, :last_played, :user_id)
  end
end
