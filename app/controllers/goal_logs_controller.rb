# frozen_string_literal: true

class GoalLogsController < ApplicationController
  before_action :set_goal, only: %i[index]
  before_action :authenticate_user!

  # GET /goals or /goals.json
  def index
    @goal_logs = @goal.goal_logs.take(25)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_goal
    @goal = Goal.find(params[:goal_id])
  end

  # Only allow a list of trusted parameters through.
  def goal_params
    params.require(:goal).permit(:value, :active)
  end
end
