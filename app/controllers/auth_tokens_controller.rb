# frozen_string_literal: true

class AuthTokensController < ApplicationController
  before_action :authenticate_user!

  def edit; end

  def update
    current_user.regenerate_auth_token
    flash[:auth_token] = current_user.auth_token
    redirect_to auth_tokens_edit_path
  end
end
