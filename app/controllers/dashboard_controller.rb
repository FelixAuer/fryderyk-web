# frozen_string_literal: true

class DashboardController < ApplicationController
  before_action :authenticate_user!

  def show # rubocop:todo Metrics/AbcSize, Metrics/MethodLength
    @today = current_user.days.today.first.time_in_seconds
    @yesterday = current_user.days.yesterday.first.time_in_seconds
    last_seven_days = current_user.days.last_7_days
    @sum7 = last_seven_days.to_a.reduce(0) { |sum, day| sum + day[:time_in_seconds] }

    last30 = current_user.days.last_30_days.sort_by(&:date).each_with_object({}) do |day, accum|
      accum[day.date] = day.time_in_seconds / 60
    end
    @last_30_days = ((Date.today - 29.days)..Date.today).each_with_object({}) do |date, hash|
      hash[date.to_s] = last30.fetch(date, 0)
    end

    date = Date.today
    @sum_month = current_user.days.where(date: (date.beginning_of_month..date.end_of_month)).sum(:time_in_seconds)
    @sum_year = current_user.days.where(date: (date.beginning_of_year..date.end_of_year)).sum(:time_in_seconds)
    @sum_all = current_user.days.sum(:time_in_seconds)

    @repertoire_piece = RepertoirePiece.get_due_today(current_user)
    @daily_goal = current_user.daily_goal
    @weekly_goal = current_user.weekly_goal

    h = {}
    ((date - 1.year).beginning_of_week..date).each { |x| h[x.strftime] = 0 }

    days = current_user.days.where(date: ((date - 1.year).beginning_of_week..date))
    days.each { |day| h[day.date.strftime] = day.time_in_seconds }
    @div = h.values.max / 5
    @h = h
  end
end
