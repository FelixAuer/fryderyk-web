# frozen_string_literal: true

class DaysController < ApplicationController
  include ApiAuthentication

  skip_forgery_protection

  before_action :authenticate_via_auth_token, only: [:store]

  def store
    days = params.permit(days: %i[date time_in_seconds]).require(:days)
    current_user.days.upsert_all(days, unique_by: %i[date user_id]) # rubocop:todo Rails/SkipsModelValidations
    GoalLog.log_achievements(current_user)
  end
end
