# frozen_string_literal: true

class PagesController < ApplicationController
  def home
    redirect_to practice_plans_path if current_user
  end

  def howto; end
end
