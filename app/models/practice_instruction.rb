# frozen_string_literal: true

class PracticeInstruction < ApplicationRecord
  belongs_to :practice_plan
  validates :name, presence: true
  validates :description, presence: true
end
