# frozen_string_literal: true

class DailyGoal < Goal
  belongs_to :user

  def self.model_name
    Goal.model_name
  end

  def self.current_time_identifier
    past_time_identifier 0
  end

  def self.past_time_identifier(intervals_in_past)
    get_time_identifier(Date.today - intervals_in_past.days)
  end

  def self.get_time_identifier(date)
    "daily_#{date.strftime('%Y-%m-%d')}"
  end

  def current_value(date = Date.today)
    user.days.where(date: date.all_day).first.time_in_seconds
  end

  def skip_period(past)
    user.absence_periods.where('starts_at <= ?', Date.today - past.days).where('ends_at >= ?',
                                                                               Date.today - past.days).first
  end
end
