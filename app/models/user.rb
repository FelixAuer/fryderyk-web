# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_secure_token :auth_token

  has_many :days, dependent: :delete_all
  has_many :repertoire_pieces, dependent: :delete_all
  has_many :repertoire_piece_practices, through: :repertoire_pieces
  has_many :goals # rubocop:todo Rails/HasManyOrHasOneDependent
  has_many :absence_periods # rubocop:todo Rails/HasManyOrHasOneDependent
  has_one :daily_goal, dependent: :delete
  has_one :weekly_goal, dependent: :delete

  validates :username, format: { with: /\A[A-Za-z0-9\-_\\.]+\z/ }

  def create_auth_token
    self.auth_token = signed_id
    save
    Rails.logger.debug auth_token
  end
end
