# frozen_string_literal: true

class RepertoirePiece < ApplicationRecord
  belongs_to :user
  has_many :repertoire_piece_practices, dependent: :delete_all

  # days between repetitions for different levels
  # should be enough lol
  DAYS_INTERVALS = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597].freeze

  after_initialize { |piece| piece.due_at ||= Date.today if new_record? }

  def self.get_due_today(user)
    practice_session = user.repertoire_piece_practices.where(created_at: Date.today.all_day).first
    return practice_session.repertoire_piece if practice_session

    user.repertoire_pieces.where('due_at <= ?', Date.today).min_by do |practice|
      practice.last_played ? practice.last_played.to_time.to_i : 0
    end
  end

  def revised_today?
    repertoire_piece_practices.where(created_at: Date.today.all_day).count.positive?
  end

  def raise_level
    log_practice level + 1
  end

  def lower_level
    log_practice level - 1
  end

  def keep_level
    log_practice level
  end

  def rollback_practice
    practice = repertoire_piece_practices.where(created_at: Date.today.all_day).first
    practice&.delete
    self.due_at = Date.today
    save!
  end

  def level
    return 4 unless last_practice

    last_practice.level
  end

  def last_played
    return nil unless last_practice

    last_practice.created_at.to_date
  end

  def last_practice
    repertoire_piece_practices.order(created_at: :desc).first
  end

  private

  def log_practice(new_level)
    new_level = 1 if new_level < 1
    repertoire_piece_practices.create(level: new_level, level_change: new_level - level)
    set_due_at
    save!
  end

  def set_due_at
    self.due_at = DAYS_INTERVALS[level - 1].days.from_now
  end
end
