# frozen_string_literal: true

class WeeklyGoal < Goal
  belongs_to :user

  def self.model_name
    Goal.model_name
  end

  def current_value(date = Date.today)
    user.days.where(date: (date.beginning_of_week..date.end_of_week)).sum(:time_in_seconds)
  end

  def self.current_time_identifier
    past_time_identifier 0
  end

  def self.past_time_identifier(intervals_in_past)
    get_time_identifier(Date.today - intervals_in_past.days * 7)
  end

  def self.get_time_identifier(date)
    "weekly_#{date.strftime('%GW%V')}"
  end
end
