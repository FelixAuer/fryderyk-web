# frozen_string_literal: true

class Goal < ApplicationRecord
  belongs_to :user
  has_many :goal_logs, dependent: :delete_all
  validates :value, presence: true, numericality: { only_integer: true, greater_than: 0 }

  after_save do |goal|
    goal.current_log&.delete
    goal.log_achievement
  end

  def log_achievement(date = Date.today)
    return unless reached?(date)

    goal_logs.find_or_create_by(value: value,
                                time_identifier: self.class.get_time_identifier(date))
  end

  def current_log
    goal_logs.where(time_identifier: self.class.current_time_identifier).first
  end

  def reached?(date = Date.today)
    current_value(date) >= value
  end

  def percentage
    return 100 if reached?

    100 * current_value / value
  end

  def streak_active?
    streak_length.positive?
  end

  def streak_prolonged?
    reached?
  end

  def streak_length # rubocop:todo Metrics/MethodLength
    length = 0
    length += 1 if streak_prolonged?
    log = true
    past = 1
    while log
      log = goal_logs.where(time_identifier: self.class.past_time_identifier(past)).first
      if log
        length += 1
        past += 1
      end
      if skip_period(past)
        log = true
        past += 1
      end
    end
    length
  end

  def skip_period(_past)
    false
  end
end
