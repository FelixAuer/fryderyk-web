# frozen_string_literal: true

class PracticePlan < ApplicationRecord
  belongs_to :user
  has_many :practice_instructions, dependent: :delete_all

  validates :name, presence: true
  validates :description, presence: true
  validates :intro, presence: true
  validates :duration, presence: true

  accepts_nested_attributes_for :practice_instructions, update_only: true

  def duplicate
    new_practice_plan = dup
    new_practice_plan.name = "Copy of #{name}"
    new_practice_plan.save!
    new_practice_plan.practice_instructions.create(
      practice_instructions.map do |instruction|
        instruction.attributes.except('id', 'created_at', 'updated_at')
      end.to_a
    )
    new_practice_plan
  end
end
