# frozen_string_literal: true

class AbsencePeriod < ApplicationRecord
  belongs_to :user

  scope :active, -> { where('starts_at <= ?', Date.today).where('ends_at >= ?', Date.today) }
end
