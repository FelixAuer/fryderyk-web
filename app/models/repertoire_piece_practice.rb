# frozen_string_literal: true

class RepertoirePiecePractice < ApplicationRecord
  belongs_to :repertoire_piece
end
