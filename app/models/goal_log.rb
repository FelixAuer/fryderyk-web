# frozen_string_literal: true

class GoalLog < ApplicationRecord
  belongs_to :goal

  def self.log_achievements(user, date = Date.today)
    user.goals.each do |goal|
      goal.log_achievement date
    end
  end
end
