# frozen_string_literal: true

class Day < ApplicationRecord
  scope :today, -> { where(date: Date.today.all_day) }
  scope :yesterday, -> { where(date: Date.yesterday.all_day) }
  scope :last_7_days, -> { where(date: (Date.today - 6.days)..Date.today) }
  scope :last_30_days, -> { where(date: (Date.today - 29.days)..Date.today) }

  belongs_to :user
end

class ::NilClass
  def time_in_seconds
    0
  end
end
