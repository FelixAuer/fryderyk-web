# frozen_string_literal: true

namespace :goals do
  desc 'Recalculates if goals are reached for all users'
  task recalculate: :environment do
    users = User.all
    users.each do |user|
      user.goals.all.each do |goal|
        goal.goal_logs.delete_all
      end
      user.days.all.each do |day|
        user.goals.all.each do |goal|
          pp "Checking Goal for #{day.date}"
          pp "#{goal.class.get_time_identifier(day.date)} reached for #{day.date}" if goal.reached?(day.date)
          goal.log_achievement(day.date)
        end
      end
    end
  end
end
