# frozen_string_literal: true

require 'test_helper'

class AbsencePeriodsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    user = create(:user)
    sign_in user
    user.absence_periods.create(starts_at: Date.yesterday, ends_at: Date.tomorrow)
    @absence_period = user.absence_periods.first
  end

  test 'should get index' do
    get absence_periods_url
    assert_response :success
  end

  test 'should get new' do
    get new_absence_period_url
    assert_response :success
  end

  test 'should create absence_period' do
    assert_difference('AbsencePeriod.count') do
      post absence_periods_url,
           params: { absence_period: { ends_at: @absence_period.ends_at, starts_at: @absence_period.starts_at } }
    end

    assert_redirected_to absence_periods_url
  end

  test 'should show absence_period' do
    get absence_period_url(@absence_period)
    assert_response :success
  end

  test 'should get edit' do
    get edit_absence_period_url(@absence_period)
    assert_response :success
  end

  test 'should update absence_period' do
    patch absence_period_url(@absence_period),
          params: { absence_period: { ends_at: @absence_period.ends_at, starts_at: @absence_period.starts_at,
                                      user_id: @absence_period.user_id } }
    assert_redirected_to absence_period_url(@absence_period)
  end

  test 'should destroy absence_period' do
    assert_difference('AbsencePeriod.count', -1) do
      delete absence_period_url(@absence_period)
    end

    assert_redirected_to absence_periods_url
  end
end
