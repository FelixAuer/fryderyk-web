# frozen_string_literal: true

require 'test_helper'

class UserProfilesControllerTest < ActionDispatch::IntegrationTest
  test 'trying to see a non existent users profile returns a 404' do
    assert_raises(ActionController::RoutingError) do
      get user_profiles_show_url('non-existent-username')
    end
  end

  test 'you can see a user public profile page' do
    user = create(:user)
    get user_profiles_show_url(user.username)
    assert_response :success
  end

  test 'the username is case insensitive' do
    user = create(:user)
    get user_profiles_show_url(user.username.upcase)
    assert_response :success
  end
end
