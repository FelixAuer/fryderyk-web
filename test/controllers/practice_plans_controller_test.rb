# frozen_string_literal: true

require 'test_helper'

class PracticePlansControllerTest < ActionDispatch::IntegrationTest # rubocop:todo Metrics/ClassLength
  include Devise::Test::IntegrationHelpers

  setup do
    @practice_plan = create :practice_plan
    sign_in @practice_plan.user
  end

  test 'should get index' do
    get practice_plans_url
    assert_response :success
  end

  test 'should get new' do
    get new_practice_plan_url
    assert_response :success
  end

  test 'should create practice_plan' do
    assert_difference('PracticePlan.count') do
      post practice_plans_url,
           params: { practice_plan: { name: 'test', description: 'this is a practrice plan',
                                      intro: 'this will be the first card', duration: 'seas olta' } }
    end

    assert_redirected_to edit_practice_plan_url(PracticePlan.last)
  end

  test 'should show practice_plan' do
    get practice_plan_url(@practice_plan)
    assert_response :success
  end

  test 'should get edit' do
    get edit_practice_plan_url(@practice_plan)
    assert_response :success
  end

  test 'should update practice_plan' do
    patch practice_plan_url(@practice_plan),
          params: { practice_plan: { name: 'asdf', description: 'asdfdsaf', intro: 'dsfd' } }
    assert_redirected_to practice_plan_url(@practice_plan)
  end

  test 'should destroy practice_plan' do
    assert_difference('PracticePlan.count', -1) do
      delete practice_plan_url(@practice_plan)
    end

    assert_redirected_to practice_plans_url
  end

  test 'you can add instructions via the store method' do
    practice_plan = create :practice_plan
    put practice_plan_url(practice_plan), params: {
      practice_plan: {
        name: 'asdf',
        description: 'asdfdsaf',
        intro: 'dsfd',
        practice_instructions_attributes: [
          {
            name: 'test',
            description: 'push key to make sound',
            end_condition: 'ssdfsdf',
            position: 1
          },
          {
            name: 'test2',
            description: 'push key to make sound asdf',
            end_condition: 'la le lu',
            position: 2
          }
        ]
      }
    }

    assert_equal 2, practice_plan.practice_instructions.count
  end

  test 'instructions dont get deleted if an error happens' do # rubocop:todo Metrics/BlockLength
    practice_plan = create :practice_plan
    put practice_plan_url(practice_plan), params: {
      practice_plan: {
        name: 'asdf',
        description: 'asdfdsaf',
        intro: 'dsfd',
        practice_instructions_attributes: [
          {
            name: 'test',
            description: 'push key to make sound',
            end_condition: 'ssdfsdf',
            position: 1
          },
          {
            name: 'test2',
            description: 'push key to make sound asdf',
            end_condition: 'la le lu',
            position: 2
          }
        ]
      }
    }

    assert_equal 2, practice_plan.practice_instructions.count

    put practice_plan_url(practice_plan), params: {
      practice_plan: {
        name: nil,
        description: 'asdfdsaf',
        intro: 'dsfd',
        practice_instructions_attributes: [
          {
            name: 'test',
            description: 'push key to make sound',
            end_condition: 'ssdfsdf',
            position: 1
          }
        ]
      }
    }

    # the request above should not destroy the two existing instructions
    assert_equal 2, practice_plan.practice_instructions.count
  end

  test "you cant delete another person's practice plan" do
    practice_plan = create :practice_plan
    sign_in create(:user)
    delete practice_plan_url(practice_plan)
    assert_response :forbidden
  end

  test 'you can duplicate a practice plan' do
    practice_plan = create :practice_plan
    sign_in practice_plan.user

    assert_difference('PracticePlan.count', 1) do
      post duplicate_practice_plan_url(practice_plan)
    end
  end
end
