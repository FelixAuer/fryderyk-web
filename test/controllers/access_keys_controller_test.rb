# frozen_string_literal: true

require 'test_helper'

class AuthTokenControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'a logged in user should get edit' do
    sign_in create(:user)
    get auth_tokens_edit_url
    assert_response :success
  end

  test 'a guest should not get edit' do
    get auth_tokens_edit_url
    assert_response :redirect
  end

  test 'posting to store changes the users access key' do
    user = create(:user)
    auth_token = user.auth_token
    sign_in user
    post auth_tokens_update_url
    user.reload
    assert_not_equal auth_token, user.auth_token
  end

  test 'posting to store redirects to the edit page and shows the new token' do
    user = create(:user)
    sign_in user
    post auth_tokens_update_url
    user.reload
    assert_response :redirect
    follow_redirect!

    assert_select '#auth_token', user.auth_token
  end
end
