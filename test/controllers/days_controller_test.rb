# frozen_string_literal: true

require 'test_helper'

class DaysControllerTest < ActionDispatch::IntegrationTest
  test 'posting days persists them' do
    user = create(:user)
    days = [
      {
        date: '2022-06-22',
        time_in_seconds: 30
      },
      {
        date: '2022-06-21',
        time_in_seconds: 60
      },
      {
        date: '2022-06-19',
        time_in_seconds: 120
      }
    ]
    post '/days', params: { days: days, auth_token: user.auth_token }
    assert_response :success

    assert_equal 3, user.days.all.count
  end

  test 'posting the same day twice updates it' do
    user = create(:user)
    date = '2022-06-22'
    day = user.days.create({ date: date, time_in_seconds: 30 })

    new_time_in_seconds = 60
    post '/days', params: { days: [{ date: date, time_in_seconds: new_time_in_seconds }], auth_token: user.auth_token }

    assert_equal 1, user.days.all.count
    day.reload
    assert_equal new_time_in_seconds, day.time_in_seconds
  end

  test 'posting to the route logs reached goals' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    post '/days', params: { days: [{ date: Date.today, time_in_seconds: 1500 }], auth_token: user.auth_token }

    assert_not_nil user.daily_goal.current_log
  end

  test 'you cant post a day with a wrong auth token' do
    assert_raises(ActionController::RoutingError) do
      post '/days', params: { days: [{ date: '2022-06-22', time_in_seconds: 60 }], auth_token: 'JAJAJAJAJA' }
    end
  end
end
