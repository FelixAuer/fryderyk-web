# frozen_string_literal: true

require 'test_helper'
class RepertoirePiecesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "you can raise a piece's level via url" do
    user = create :user
    piece = user.repertoire_pieces.create(name: 'für elise', composer: 'beethoven')
    piece.repertoire_piece_practices.create(level: 1)
    sign_in user

    post raise_level_repertoire_piece_url(piece)

    piece.reload
    assert_equal 2, piece.level
  end

  test "you can lower a piece's level via url" do
    user = create :user
    piece = user.repertoire_pieces.create(name: 'für elise', composer: 'beethoven')
    piece.repertoire_piece_practices.create(level: 3)

    sign_in user

    post lower_level_repertoire_piece_url(piece)

    piece.reload
    assert_equal 2, piece.level
  end

  test "you can keep a piece's level via url" do
    user = create :user
    piece = user.repertoire_pieces.create(name: 'für elise', composer: 'beethoven')
    piece.repertoire_piece_practices.create(level: 3)

    sign_in user

    post keep_level_repertoire_piece_url(piece)

    piece.reload
    assert_equal 3, piece.level
  end

  test "you can rollback a piece's practice via url" do
    user = create :user
    piece = user.repertoire_pieces.create(name: 'für elise', composer: 'beethoven')
    piece.repertoire_piece_practices.create(level: 3, created_at: 1.day.before.to_date)
    piece.lower_level
    assert_equal 2, piece.level
    sign_in user

    post rollback_practice_repertoire_piece_url(piece)

    piece.reload
    assert_equal 3, piece.level
  end
end
