# frozen_string_literal: true

require 'test_helper'

class AbsencePeriodTest < ActiveSupport::TestCase
  test 'an absence period keeps a the daily streak alive, but doesnt prolong the count' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    date = Date.today - 3.days
    user.days.create([time_in_seconds: 6000, date: date])
    GoalLog.log_achievements(user, date)

    user.absence_periods.create(starts_at: Date.today - 2.days, ends_at: Date.tomorrow)

    assert_equal true, user.daily_goal.streak_active?
    assert_equal false, user.daily_goal.streak_prolonged?
    assert_equal 1, user.daily_goal.streak_length
  end

  test 'it works on the last day of the absence period' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    date = Date.today - 3.days
    user.days.create([time_in_seconds: 6000, date: date])
    GoalLog.log_achievements(user, date)

    user.absence_periods.create(starts_at: Date.today - 2.days, ends_at: Date.today)

    assert_equal true, user.daily_goal.streak_active?
    assert_equal false, user.daily_goal.streak_prolonged?
    assert_equal 1, user.daily_goal.streak_length
  end

  test 'an absence period in the middle of a series of reached goals does what it should do' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    user.daily_goal.goal_logs.create(time_identifier: DailyGoal.past_time_identifier(10))
    user.daily_goal.goal_logs.create(time_identifier: DailyGoal.past_time_identifier(9))
    user.daily_goal.goal_logs.create(time_identifier: DailyGoal.past_time_identifier(8))
    user.daily_goal.goal_logs.create(time_identifier: DailyGoal.past_time_identifier(7))
    user.absence_periods.create(starts_at: Date.today - 6.days, ends_at: Date.today - 4.days)
    user.daily_goal.goal_logs.create(time_identifier: DailyGoal.past_time_identifier(3))
    user.daily_goal.goal_logs.create(time_identifier: DailyGoal.past_time_identifier(2))
    user.daily_goal.goal_logs.create(time_identifier: DailyGoal.past_time_identifier(1))

    assert_equal true, user.daily_goal.streak_active?
    assert_equal false, user.daily_goal.streak_prolonged?
    assert_equal 7, user.daily_goal.streak_length
  end
end
