# frozen_string_literal: true

require 'test_helper'

class RepertoirePieceTest < ActiveSupport::TestCase # rubocop:todo Metrics/ClassLength
  test 'you can attach a piece to a user' do
    user = create :user
    user.repertoire_pieces.create(name: 'waltz in a minor', composer: 'chopin')
    user.reload
    assert_equal 1, user.repertoire_pieces.count
  end

  test 'the default level for a new piece is 4' do
    piece = create_piece
    assert_equal 4, piece.level
  end

  test 'a new piece is due today' do
    piece = create_piece
    assert_equal Date.today, piece.due_at
  end

  test "keeping a new piece's level makes it due in 5 days" do
    piece = create_piece
    piece.keep_level
    assert_equal 5.days.from_now.to_date, piece.due_at
  end

  test 'pushing a level 1 piece raises the level' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 1)
    piece.raise_level
    piece.reload
    assert_equal 2, piece.level
  end

  test 'pushing a level 2 piece raises the level' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 2)
    piece.save

    piece.raise_level
    piece.reload
    assert_equal 3, piece.level
  end

  test 'pushing a level 3 piece raises the level' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 3)
    piece.save

    piece.raise_level
    piece.reload
    assert_equal 4, piece.level
  end

  test 'pushing a level 1 piece makes it due in 2 days' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 1)
    piece.raise_level
    piece.reload
    assert_equal 2.days.from_now.to_date, piece.due_at
  end

  test 'pushing a level 2 piece makes it due in 3 days' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 2)
    piece.save

    piece.raise_level
    piece.reload
    assert_equal 3.days.from_now.to_date, piece.due_at
  end

  test 'pushing a level 3 piece makes it due in 5 days' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 3)
    piece.save

    piece.raise_level
    piece.reload
    assert_equal 5.days.from_now.to_date, piece.due_at
  end

  test 'pushing a level 4 piece makes it due in 8 days' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 4)
    piece.save

    piece.raise_level
    piece.reload
    assert_equal 8.days.from_now.to_date, piece.due_at
  end

  test 'pushing a level 5 piece makes it due in 13 days' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 5)
    piece.save

    piece.raise_level
    piece.reload
    assert_equal 13.days.from_now.to_date, piece.due_at
  end

  test 'you can lower a pieces level' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 3)
    piece.save

    piece.lower_level
    piece.reload
    assert_equal 2, piece.level
    assert_equal 2.days.from_now.to_date, piece.due_at
  end

  test 'you can keep a pieces level' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 3)
    piece.save

    piece.keep_level
    piece.reload
    assert_equal 3, piece.level
  end

  test 'lowering a level 1 pieces level does not lower its level' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 1)
    piece.save

    piece.lower_level
    piece.reload
    assert_equal 1, piece.level
  end

  test 'pushing a new piece sets last_played to today' do
    piece = create_piece
    piece.raise_level
    piece.reload
    assert_equal Date.today, piece.last_played
  end

  test 'lowering a pieces level sets last_played to today' do
    piece = create_piece
    piece.lower_level
    piece.reload
    assert_equal Date.today, piece.last_played
  end

  test 'keeping a pieces level sets last_played to today' do
    piece = create_piece
    piece.keep_level
    piece.reload
    assert_equal Date.today, piece.last_played
  end

  test 'keeping a pieces level logs it at the practice object' do
    piece = create_piece
    piece.keep_level
    piece.reload
    assert_equal 0, piece.last_practice.level_change
  end

  test 'raising a pieces level logs it at the practice object' do
    piece = create_piece
    piece.raise_level
    piece.reload
    assert_equal 1, piece.last_practice.level_change
  end

  test 'lowering a pieces level logs it at the practice object' do
    piece = create_piece
    piece.repertoire_piece_practices.create(level: 3)
    piece.lower_level
    piece.reload
    assert_equal(-1, piece.last_practice.level_change)
  end

  test 'you can check if a piece was today' do
    piece = create_piece
    assert_equal false, piece.revised_today?

    piece.raise_level
    assert_equal true, piece.revised_today?
  end

  test 'you can get the piece that is due today for a user' do
    piece = create_piece
    assert_equal piece, RepertoirePiece.get_due_today(piece.user)
  end

  test 'if a piece was due yesterday you get it today' do
    user = create :user
    piece = user.repertoire_pieces.create(name: 'für elise', composer: 'beethoven', due_at: Date.yesterday.to_date)
    assert_equal piece, RepertoirePiece.get_due_today(piece.user)
  end

  test 'if a user already practiced today they get the same piece' do
    user = create :user
    user.repertoire_pieces.create(name: 'waltz in a minor', composer: 'chopin')
    piece = RepertoirePiece.get_due_today(user)
    piece.raise_level
    assert_equal piece, RepertoirePiece.get_due_today(user)
  end

  test "if there are several pieces due today you get the one that hasn't been played the longest" do
    user = create :user
    user.repertoire_pieces.create(name: 'waltz in a minor',
                                  # rubocop:todo Layout/LineLength
                                  composer: 'chopin').repertoire_piece_practices.create(created_at: 10.days.before.to_date)
    # rubocop:enable Layout/LineLength
    piece = user.repertoire_pieces.create(name: 'für elise', composer: 'beethoven')
    # rubocop:todo Layout/LineLength
    piece.repertoire_piece_practices.create(created_at: 15.days.before.to_date) # use second one to not have insertion order change anything
    # rubocop:enable Layout/LineLength
    user.repertoire_pieces.create(name: 'für elise',
                                  # rubocop:todo Layout/LineLength
                                  composer: 'beethoven').repertoire_piece_practices.create(created_at: 1.day.before.to_date)
    # rubocop:enable Layout/LineLength
    assert_equal piece, RepertoirePiece.get_due_today(piece.user)
  end

  test "if there are several pieces due today you get the one that hasn't been played at all" do
    user = create :user
    user.repertoire_pieces.create(name: 'waltz in a minor',
                                  # rubocop:todo Layout/LineLength
                                  composer: 'chopin').repertoire_piece_practices.create(created_at: 10.days.before.to_date)
    # rubocop:enable Layout/LineLength
    piece = user.repertoire_pieces.create(name: 'für elise', composer: 'beethoven')
    user.repertoire_pieces.create(name: 'für elise',
                                  # rubocop:todo Layout/LineLength
                                  composer: 'beethoven').repertoire_piece_practices.create(created_at: 1.day.before.to_date)
    # rubocop:enable Layout/LineLength
    assert_equal piece, RepertoirePiece.get_due_today(piece.user)
  end

  test 'you can rollback the practice session of the day' do
    user = create :user
    piece = user.repertoire_pieces.create(name: 'für elise', composer: 'beethoven')
    piece.repertoire_piece_practices.create(created_at: 1.day.before.to_date, level: 3)

    piece.lower_level
    piece.rollback_practice
    assert_equal 3, piece.level
    assert_equal Date.today, piece.due_at
  end

  def create_piece
    user = create :user
    user.repertoire_pieces.create(name: 'waltz in a minor', composer: 'chopin')
  end
end
