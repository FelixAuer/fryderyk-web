# frozen_string_literal: true

require 'test_helper'

class DayTest < ActiveSupport::TestCase
  test 'you can create a day for a user' do
    user = create(:user)
    assert_empty user.days

    user.days.create([time_in_seconds: 60, date: Date.today])
    assert_not_empty user.days
  end
end
