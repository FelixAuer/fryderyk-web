# frozen_string_literal: true

require 'test_helper'

class GoalLogTest < ActiveSupport::TestCase
  test 'you can log a reached goal' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    user.days.create([time_in_seconds: 1500, date: Date.today])

    user.daily_goal.log_achievement

    assert_not_nil user.daily_goal.goal_logs.first
  end

  test "an unreached goal isn't logged" do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)

    user.daily_goal.log_achievement

    assert_nil user.daily_goal.goal_logs.first
  end

  test 'you can log all of a users achieved current goals' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 100)
    user.weekly_goal = WeeklyGoal.create(value: 1300)
    user.days.create([time_in_seconds: 1500, date: Date.today])

    GoalLog.log_achievements(user)

    assert_not_nil user.daily_goal.goal_logs.first
    assert_not_nil user.weekly_goal.goal_logs.first
  end

  test 'updating a goal deletes the log and relogs it if it is reached' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    user.days.create([time_in_seconds: 1500, date: Date.today])

    user.daily_goal.log_achievement
    assert_not_nil user.daily_goal.goal_logs.first

    user.daily_goal.value = 2000
    user.daily_goal.save
    assert_nil user.daily_goal.goal_logs.first

    user.daily_goal.value = 200
    user.daily_goal.save
    assert_not_nil user.daily_goal.goal_logs.first
  end

  test 'you can get a goals current log' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    user.days.create([time_in_seconds: 1500, date: Date.today])
    user.daily_goal.log_achievement

    assert_equal user.daily_goal.goal_logs.first, user.daily_goal.current_log
  end

  test 'the current log of an unreached goal is nil' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    user.daily_goal.log_achievement

    assert_nil user.daily_goal.current_log
  end

  test 'you can log a goal for a specific day' do
    date = Date.parse '2022-03-06'
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    user.days.create([time_in_seconds: 6000, date: date])
    user.daily_goal.log_achievement date

    assert_not_nil user.daily_goal.goal_logs.where(time_identifier: 'daily_2022-03-06')
  end
end
