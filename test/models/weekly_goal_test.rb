# frozen_string_literal: true

require 'test_helper'

class WeeklyGoalTest < ActiveSupport::TestCase
  # throw the whole behaviour we want in a testcase and work our way to it
  test 'a users daily goal can tell us if it is reached and to how many percentages it is reached' do
    user = create(:user)
    user.weekly_goal = WeeklyGoal.create(value: 1000)

    assert_equal false, user.weekly_goal.reached?
    assert_equal 0, user.weekly_goal.percentage

    user.days.create([time_in_seconds: 450, date: Date.today])

    assert_equal false, user.weekly_goal.reached?
    assert_equal 45, user.weekly_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 1000, date: Date.today])

    assert_equal true, user.weekly_goal.reached?
    assert_equal 100, user.weekly_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 1500, date: Date.today])

    assert_equal true, user.weekly_goal.reached?
    # once the goal is reached we dont care how much we overshoot
    assert_equal 100, user.weekly_goal.percentage
  end

  test 'you can get a users daily goal' do
    user = create(:user)
    assert_nil user.weekly_goal

    user.weekly_goal = WeeklyGoal.create(value: 1000)
    assert_not_nil user.weekly_goal
  end

  test 'you can check if a users daily goal is reached' do
    user = create(:user)
    user.weekly_goal = WeeklyGoal.create(value: 1000)

    assert_equal false, user.weekly_goal.reached?

    user.days.create([time_in_seconds: 1000, date: Date.today])

    assert_equal true, user.weekly_goal.reached?
  end

  test 'you can get the percentage of how much of the goal is achieved' do
    user = create(:user)

    user.weekly_goal = WeeklyGoal.create(value: 1000)
    assert_equal 0, user.weekly_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 50, date: Date.today.beginning_of_week])
    user.days.create([time_in_seconds: 100, date: Date.today.beginning_of_week + 1.day])
    assert_equal 15, user.weekly_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 500, date: Date.today.beginning_of_week])
    user.days.create([time_in_seconds: 500, date: Date.today.beginning_of_week + 1.day])
    assert_equal 100, user.weekly_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 1500, date: Date.today.beginning_of_week + 1.day])
    assert_equal 100, user.weekly_goal.percentage
  end

  test "only this weeks' values are relevant" do
    user = create(:user)

    user.weekly_goal = WeeklyGoal.create(value: 1000)
    assert_equal 0, user.weekly_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 150, date: Date.today.beginning_of_week])
    user.days.create([time_in_seconds: 150, date: Date.today.beginning_of_week - 1.day])
    assert_equal 15, user.weekly_goal.percentage
  end

  test 'you can get the current timespan identifier of a weekly goal' do
    assert_equal "weekly_#{Date.today.strftime('%GW%V')}", WeeklyGoal.current_time_identifier
  end

  test 'you can check if a goal was reached in the past for a specific day' do
    date = Date.parse '2022-03-06'
    user = create(:user)
    user.weekly_goal = WeeklyGoal.create(value: 1000)
    user.days.create([time_in_seconds: 6000, date: date])

    assert_equal true, user.weekly_goal.reached?(date)
  end
end
