# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'a user has an auth token' do
    user = create(:user)
    assert_not_nil user.auth_token
  end

  test 'you can regenerate the auth token' do
    user = create(:user)
    auth_token = user.auth_token
    user.regenerate_auth_token
    assert_not_equal auth_token, user.auth_token
  end

  test 'you can find a user by its auth token' do
    user = create(:user)
    found = User.find_by(auth_token: user.auth_token)
    assert_equal user, found
  end

  test 'if the auth token doesnt exist it returns nil' do
    found = User.find_by(auth_token: 'asdf123')
    assert_nil found
  end

  test 'you can change the username to an alphanumeric name' do
    user = create(:user)
    user.username = 'xXxCHOPINDESTROYER420xXx'
    user.save!
  end

  test 'the username can contain dots, hyphens and underscores' do
    user = create(:user)
    user.username = 'i-am_the.best'
    user.save!
  end

  test 'you can not change the username to something with a space' do
    user = create(:user)
    user.username = 'this name is forbidden'
    assert_raises(ActiveRecord::RecordInvalid) do
      user.save!
    end
  end
end
