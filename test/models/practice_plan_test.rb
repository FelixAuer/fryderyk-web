# frozen_string_literal: true

require 'test_helper'

class PracticePlanTest < ActiveSupport::TestCase
  test 'you can duplicate a practice plan' do
    practice_plan = create(:practice_plan)
    assert_equal 1, PracticePlan.count

    practice_plan.duplicate
    assert_equal 2, PracticePlan.count
  end

  test 'duplicating a practice plan copies all attributes but the id' do
    practice_plan = create(:practice_plan)
    duplicated_practice_plan = practice_plan.duplicate
    assert_equal practice_plan.description, duplicated_practice_plan.description
    assert_equal practice_plan.intro, duplicated_practice_plan.intro
    assert_equal practice_plan.user_id, duplicated_practice_plan.user_id

    assert_not_equal practice_plan.id, duplicated_practice_plan.id
  end

  test 'duplicating a practice plan adjusts the name' do
    practice_plan = create(:practice_plan)
    duplicated_practice_plan = practice_plan.duplicate
    assert_equal "Copy of #{practice_plan.name}", duplicated_practice_plan.name
  end

  test 'duplicating a practice plan duplicates its instructions' do
    practice_plan = create(:practice_plan)
    practice_plan.practice_instructions.create(
      name: 'asdf', description: 'asdfsdafsda', end_condition: 'lollo rosso best salad'
    )
    practice_plan.practice_instructions.create(
      name: 'asdf2', description: 'asdfsdafsda2', end_condition: 'vogerlsalat is a net schlecht'
    )

    duplicated_practice_plan = practice_plan.duplicate

    assert_equal 4, PracticeInstruction.count
    assert_equal 2, duplicated_practice_plan.practice_instructions.count
  end
end
