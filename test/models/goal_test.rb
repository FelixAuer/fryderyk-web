# frozen_string_literal: true

require 'test_helper'

class GoalTest < ActiveSupport::TestCase
  test 'a daily goal that was not reached today or yesterday does not have a streak' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)

    assert_equal false, user.daily_goal.streak_active?
    assert_equal false, user.daily_goal.streak_prolonged?
    assert_equal 0, user.daily_goal.streak_length
  end

  test 'a daily goal that was reached today has a 1 day streak' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    user.days.create([time_in_seconds: 6000, date: Date.today])

    assert_equal true, user.daily_goal.streak_active?
    assert_equal true, user.daily_goal.streak_prolonged?
    assert_equal 1, user.daily_goal.streak_length
  end

  test 'a daily goal that was reached yesterday but not today has an active but not prolonged 1 day streak' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    user.daily_goal.goal_logs.create(time_identifier: DailyGoal.past_time_identifier(1))

    assert_equal true, user.daily_goal.streak_active?
    assert_equal false, user.daily_goal.streak_prolonged?
    assert_equal 1, user.daily_goal.streak_length
  end

  test 'a daily goal that was reached yesterday and today has an active and prolonged 2 day streak' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    user.days.create([time_in_seconds: 6000, date: Date.today])
    GoalLog.log_achievements(user)
    user.daily_goal.goal_logs.create(time_identifier: DailyGoal.past_time_identifier(1))

    assert_equal true, user.daily_goal.streak_active?
    assert_equal true, user.daily_goal.streak_prolonged?
    assert_equal 2, user.daily_goal.streak_length
  end

  test 'a weekly goal that was not reached today or yesterday does not have a streak' do
    user = create(:user)
    user.weekly_goal = WeeklyGoal.create(value: 1000)

    assert_equal false, user.weekly_goal.streak_active?
    assert_equal false, user.weekly_goal.streak_prolonged?
    assert_equal 0, user.weekly_goal.streak_length
  end

  test 'a weekly goal that was reached this week has a 1 week streak' do
    user = create(:user)
    user.weekly_goal = WeeklyGoal.create(value: 1000)
    user.days.create([time_in_seconds: 6000, date: Date.today])

    assert_equal true, user.weekly_goal.streak_active?
    assert_equal true, user.weekly_goal.streak_prolonged?
    assert_equal 1, user.weekly_goal.streak_length
  end

  test 'a weekly goal that was reached last week but not this week has an active but not prolonged 1 week streak' do
    user = create(:user)
    user.weekly_goal = WeeklyGoal.create(value: 1000)
    user.weekly_goal.goal_logs.create(time_identifier: WeeklyGoal.past_time_identifier(1))

    assert_equal true, user.weekly_goal.streak_active?
    assert_equal false, user.weekly_goal.streak_prolonged?
    assert_equal 1, user.weekly_goal.streak_length
  end

  test 'a weekly goal that was reached yesterday and today has an active and prolonged 2 day streak' do
    user = create(:user)
    user.weekly_goal = WeeklyGoal.create(value: 1000)
    user.days.create([time_in_seconds: 6000, date: Date.today])
    GoalLog.log_achievements(user)
    user.weekly_goal.goal_logs.create(time_identifier: WeeklyGoal.past_time_identifier(1))

    assert_equal true, user.weekly_goal.streak_active?
    assert_equal true, user.weekly_goal.streak_prolonged?
    assert_equal 2, user.weekly_goal.streak_length
  end
end
