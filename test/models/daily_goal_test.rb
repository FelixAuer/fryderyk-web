# frozen_string_literal: true

require 'test_helper'

class DailyGoalTest < ActiveSupport::TestCase
  # throw the whole behaviour we want in a testcase and work our way to it
  test 'a users daily goal can tell us if it is reached and to how many percentages it is reached' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)

    assert_equal false, user.daily_goal.reached?
    assert_equal 0, user.daily_goal.percentage

    user.days.create([time_in_seconds: 450, date: Date.today])

    assert_equal false, user.daily_goal.reached?
    assert_equal 45, user.daily_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 1000, date: Date.today])

    assert_equal true, user.daily_goal.reached?
    assert_equal 100, user.daily_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 1500, date: Date.today])

    assert_equal true, user.daily_goal.reached?
    # once the goal is reached we dont care how much we overshoot
    assert_equal 100, user.daily_goal.percentage
  end

  test 'you can get a users daily goal' do
    user = create(:user)
    assert_nil user.daily_goal

    user.daily_goal = DailyGoal.create(value: 1000)
    assert_not_nil user.daily_goal
  end

  test 'you can check if a users daily goal is reached' do
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)

    assert_equal false, user.daily_goal.reached?

    user.days.create([time_in_seconds: 1000, date: Date.today])

    assert_equal true, user.daily_goal.reached?
  end

  test 'you can get the percentage of how much of the goal is achieved' do
    user = create(:user)

    user.daily_goal = DailyGoal.create(value: 1000)
    assert_equal 0, user.daily_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 150, date: Date.today])
    assert_equal 15, user.daily_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 1000, date: Date.today])
    assert_equal 100, user.daily_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 1500, date: Date.today])
    assert_equal 100, user.daily_goal.percentage
  end
  test "only today's values are relevant" do
    user = create(:user)

    user.daily_goal = DailyGoal.create(value: 1000)
    assert_equal 0, user.daily_goal.percentage

    user.days.delete_all
    user.days.create([time_in_seconds: 150, date: Date.today])
    user.days.create([time_in_seconds: 150, date: Date.yesterday])
    assert_equal 15, user.daily_goal.percentage
  end

  test 'you can get the current timespan identifier of a daily goal' do
    assert_equal "daily_#{Date.today.strftime('%Y-%m-%d')}", DailyGoal.current_time_identifier
  end

  test 'you can get the timespan identifier of a past daily goal' do
    assert_equal "daily_#{Date.yesterday.strftime('%Y-%m-%d')}", DailyGoal.past_time_identifier(1)
  end

  test 'you can check if a goal was reached in the past for a specific day' do
    date = Date.parse '2022-03-06'
    user = create(:user)
    user.daily_goal = DailyGoal.create(value: 1000)
    user.days.create([time_in_seconds: 6000, date: date])

    assert_equal true, user.daily_goal.reached?(date)
  end
end
