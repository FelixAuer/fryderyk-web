# frozen_string_literal: true

require 'test_helper'

class PracticeInstructionTest < ActiveSupport::TestCase
  test 'you can add a practice instruction to a practice plan' do
    plan = create :practice_plan
    plan.practice_instructions.create(name: 'asdf', description: 'asdfsdafsda', end_condition: 'lollo rosso best salad')
    assert_equal 1, plan.practice_instructions.count
  end
end
