# frozen_string_literal: true

require 'application_system_test_case'

class PracticePlansTest < ApplicationSystemTestCase
  setup do
    @practice_plan = practice_plans(:one)
  end

  test 'visiting the index' do
    visit practice_plans_url
    assert_selector 'h1', text: 'Practice plans'
  end

  test 'should create practice plan' do
    visit practice_plans_url
    click_on 'New practice plan'

    click_on 'Create Practice plan'

    assert_text 'Practice plan was successfully created'
    click_on 'Back'
  end

  test 'should update Practice plan' do
    visit practice_plan_url(@practice_plan)
    click_on 'Edit this practice plan', match: :first

    click_on 'Update Practice plan'

    assert_text 'Practice plan was successfully updated'
    click_on 'Back'
  end

  test 'should destroy Practice plan' do
    visit practice_plan_url(@practice_plan)
    click_on 'Destroy this practice plan', match: :first

    assert_text 'Practice plan was successfully destroyed'
  end
end
