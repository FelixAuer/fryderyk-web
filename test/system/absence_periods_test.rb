# frozen_string_literal: true

require 'application_system_test_case'

class AbsencePeriodsTest < ApplicationSystemTestCase
  setup do
    @absence_period = absence_periods(:one)
  end

  test 'visiting the index' do
    visit absence_periods_url
    assert_selector 'h1', text: 'Absence periods'
  end

  test 'should create absence period' do
    visit absence_periods_url
    click_on 'New absence period'

    fill_in 'Ends at', with: @absence_period.ends_at
    fill_in 'Starts at', with: @absence_period.starts_at
    fill_in 'User', with: @absence_period.user_id
    click_on 'Create Absence period'

    assert_text 'Absence period was successfully created'
    click_on 'Back'
  end

  test 'should update Absence period' do
    visit absence_period_url(@absence_period)
    click_on 'Edit this absence period', match: :first

    fill_in 'Ends at', with: @absence_period.ends_at
    fill_in 'Starts at', with: @absence_period.starts_at
    fill_in 'User', with: @absence_period.user_id
    click_on 'Update Absence period'

    assert_text 'Absence period was successfully updated'
    click_on 'Back'
  end

  test 'should destroy Absence period' do
    visit absence_period_url(@absence_period)
    click_on 'Destroy this absence period', match: :first

    assert_text 'Absence period was successfully destroyed'
  end
end
