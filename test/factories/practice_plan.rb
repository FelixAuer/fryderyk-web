# frozen_string_literal: true

require 'ffaker'

FactoryBot.define do
  factory :practice_plan do
    name { FFaker::Lorem.words }
    description { FFaker::Lorem.paragraph }
    intro { FFaker::Lorem.paragraphs }
    duration { FFaker::Lorem.words 2 }

    association :user, factory: :user
  end
end
