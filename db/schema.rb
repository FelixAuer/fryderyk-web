# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_10_12_111357) do
  create_table "absence_periods", force: :cascade do |t|
    t.date "starts_at", null: false
    t.date "ends_at", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_absence_periods_on_user_id"
  end

  create_table "days", force: :cascade do |t|
    t.date "date", null: false
    t.integer "time_in_seconds", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["date", "user_id"], name: "index_days_on_date_and_user_id", unique: true
    t.index ["user_id"], name: "index_days_on_user_id"
  end

  create_table "goal_logs", force: :cascade do |t|
    t.integer "value"
    t.string "time_identifier"
    t.integer "goal_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["goal_id"], name: "index_goal_logs_on_goal_id"
  end

  create_table "goals", force: :cascade do |t|
    t.string "type"
    t.integer "value"
    t.boolean "active", default: false
    t.integer "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type", "user_id"], name: "index_goals_on_type_and_user_id", unique: true
    t.index ["user_id"], name: "index_goals_on_user_id"
  end

  create_table "practice_instructions", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.text "end_condition"
    t.integer "practice_plan_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "position"
    t.integer "seconds"
    t.integer "repetitions", default: 1, null: false
    t.index ["practice_plan_id"], name: "index_practice_instructions_on_practice_plan_id"
  end

  create_table "practice_plans", force: :cascade do |t|
    t.string "name", null: false
    t.text "description", null: false
    t.text "intro", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "duration"
    t.string "tags"
    t.boolean "show_to_guests", default: false
    t.index ["user_id"], name: "index_practice_plans_on_user_id"
  end

  create_table "repertoire_piece_practices", force: :cascade do |t|
    t.integer "level"
    t.integer "repertoire_piece_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "level_change", default: 0
    t.index ["repertoire_piece_id"], name: "index_repertoire_piece_practices_on_repertoire_piece_id"
  end

  create_table "repertoire_pieces", force: :cascade do |t|
    t.string "name", null: false
    t.string "composer", null: false
    t.date "due_at", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_repertoire_pieces_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "auth_token"
    t.string "username", null: false
    t.index ["auth_token"], name: "index_users_on_auth_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "unique_usernames", unique: true
  end

  add_foreign_key "absence_periods", "users"
  add_foreign_key "days", "users"
  add_foreign_key "goal_logs", "goals"
  add_foreign_key "goals", "users"
  add_foreign_key "practice_instructions", "practice_plans"
  add_foreign_key "practice_plans", "users"
  add_foreign_key "repertoire_piece_practices", "repertoire_pieces"
  add_foreign_key "repertoire_pieces", "users"
end
