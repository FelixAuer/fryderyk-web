# frozen_string_literal: true

class AddRequiresAccountToPracticePlans < ActiveRecord::Migration[7.0]
  def change
    add_column :practice_plans, :requires_account, :boolean
  end
end
