# frozen_string_literal: true

class CreateGoalLogs < ActiveRecord::Migration[7.0]
  def change
    create_table :goal_logs do |t|
      t.integer :value
      t.string :time_identifier
      t.references :goal, null: false, foreign_key: true

      t.timestamps
    end
  end
end
