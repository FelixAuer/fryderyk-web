# frozen_string_literal: true

class AddShowToGuestsToPracticePlans < ActiveRecord::Migration[7.0]
  def change
    add_column :practice_plans, :show_to_guests, :boolean, default: false
  end
end
