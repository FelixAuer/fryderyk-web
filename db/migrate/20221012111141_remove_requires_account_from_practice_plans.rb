# frozen_string_literal: true

class RemoveRequiresAccountFromPracticePlans < ActiveRecord::Migration[7.0]
  def change
    remove_column :practice_plans, :requires_account, :boolean
  end
end
