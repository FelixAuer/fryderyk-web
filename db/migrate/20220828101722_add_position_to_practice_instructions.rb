# frozen_string_literal: true

class AddPositionToPracticeInstructions < ActiveRecord::Migration[7.0]
  def change
    add_column :practice_instructions, :position, :integer
  end
end
