# frozen_string_literal: true

class AddRepetitionsToPracticeInstructions < ActiveRecord::Migration[7.0]
  def change
    add_column :practice_instructions, :repetitions, :integer, null: false, default: 1
  end
end
