# frozen_string_literal: true

class CreateRepertoirePieces < ActiveRecord::Migration[7.0]
  def change
    create_table :repertoire_pieces do |t|
      t.string :name, null: false
      t.string :composer, null: false
      t.integer :level, null: false, default: 1
      t.date :due_at, null: false, default: -> { 'CURRENT_DATE' }
      t.date :last_played
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
