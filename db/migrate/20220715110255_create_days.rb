# frozen_string_literal: true

class CreateDays < ActiveRecord::Migration[7.0]
  def change
    create_table :days do |t|
      t.date :date, null: false
      t.integer :time_in_seconds, null: false
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :days, %i[date user_id], unique: true
  end
end
