# frozen_string_literal: true

class AddLevelChangeToRepertoirePiecePractices < ActiveRecord::Migration[7.0]
  def change
    add_column :repertoire_piece_practices, :level_change, :integer, default: 0
  end
end
