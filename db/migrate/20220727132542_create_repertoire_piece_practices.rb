# frozen_string_literal: true

class CreateRepertoirePiecePractices < ActiveRecord::Migration[7.0]
  def change
    create_table :repertoire_piece_practices do |t|
      t.boolean :pushed
      t.integer :level
      t.references :repertoire_piece, null: false, foreign_key: true

      t.timestamps
    end
  end
end
