# frozen_string_literal: true

class AddDurationToPracticePlans < ActiveRecord::Migration[7.0]
  def change
    add_column :practice_plans, :duration, :string
  end
end
