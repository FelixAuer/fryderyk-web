# frozen_string_literal: true

class RemoveLevelAndLastPlayedFromRepertoirePieces < ActiveRecord::Migration[7.0]
  def change
    remove_column :repertoire_pieces, :level # rubocop:todo Rails/ReversibleMigration
    remove_column :repertoire_pieces, :last_played # rubocop:todo Rails/ReversibleMigration
  end
end
