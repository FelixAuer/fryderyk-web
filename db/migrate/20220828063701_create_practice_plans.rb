# frozen_string_literal: true

class CreatePracticePlans < ActiveRecord::Migration[7.0]
  def change
    create_table :practice_plans do |t|
      t.string :name, null: false
      t.text :description, null: false
      t.text :intro, null: false
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
