# frozen_string_literal: true

class AddSecondsToPracticeInstructions < ActiveRecord::Migration[7.0]
  def change
    add_column :practice_instructions, :seconds, :integer
  end
end
