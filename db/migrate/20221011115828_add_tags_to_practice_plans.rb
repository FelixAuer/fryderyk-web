# frozen_string_literal: true

class AddTagsToPracticePlans < ActiveRecord::Migration[7.0]
  def change
    add_column :practice_plans, :tags, :string
  end
end
