# frozen_string_literal: true

class CreateAbsencePeriods < ActiveRecord::Migration[7.0]
  def change
    create_table :absence_periods do |t|
      t.date :starts_at, null: false
      t.date :ends_at, null: false
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
