# frozen_string_literal: true

class RemovePushedFromRepertoirePiecePractices < ActiveRecord::Migration[7.0]
  def change
    remove_column :repertoire_piece_practices, :pushed # rubocop:todo Rails/ReversibleMigration
  end
end
