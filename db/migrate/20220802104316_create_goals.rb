# frozen_string_literal: true

class CreateGoals < ActiveRecord::Migration[7.0]
  def change
    create_table :goals do |t|
      t.string :type
      t.integer :value
      t.boolean :active, default: false
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :goals, %i[type user_id], unique: true
  end
end
