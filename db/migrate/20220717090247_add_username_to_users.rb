# frozen_string_literal: true

class AddUsernameToUsers < ActiveRecord::Migration[7.0]
  def change
    # rubocop:todo Rails/NotNullColumn
    add_column :users, :username, :string, null: false, index: { unique: true, name: 'unique_usernames' }
    # rubocop:enable Rails/NotNullColumn
  end
end
