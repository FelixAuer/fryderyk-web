# frozen_string_literal: true

class CreatePracticeInstructions < ActiveRecord::Migration[7.0]
  def change
    create_table :practice_instructions do |t|
      t.string :name
      t.text :description
      t.text :end_condition
      t.references :practice_plan, null: false, foreign_key: true

      t.timestamps
    end
  end
end
