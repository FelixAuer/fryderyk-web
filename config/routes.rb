# frozen_string_literal: true

Rails.application.routes.draw do # rubocop:todo Metrics/BlockLength
  get 'auth_tokens/edit'
  post 'auth_tokens/update'
  get 'pages/home'
  get '/dashboard', to: 'dashboard#show'
  get '/days', to: 'days#index'
  post '/days', to: 'days#store'
  delete '/', to: 'pages#home'
  get '/how-to', to: 'pages#howto'

  resources :goals do
    resources :goal_logs, only: [:index]
  end

  resources :repertoire_pieces, except: [:show] do
    member do
      post 'raise_level'
      post 'keep_level'
      post 'lower_level'
      post 'rollback_practice'
    end
  end

  resources :absence_periods
  resources :practice_plans do
    member do
      post 'duplicate'
    end
  end

  get '/u/:username', to: 'user_profiles#show', as: 'user_profiles_show'
  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root 'pages#home'
end
